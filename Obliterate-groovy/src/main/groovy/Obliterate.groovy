import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.atomic.AtomicInteger

import static java.lang.String.format

class Obliterate {
    static final AtomicInteger deletedFileCount = new AtomicInteger()
    static final AtomicInteger openFolderCount = new AtomicInteger()
    static final Executor executor = Executors.newWorkStealingPool()
    static final LinkedBlockingDeque<File> files = new LinkedBlockingDeque<File>()
    static final LinkedBlockingDeque<File> folders = new LinkedBlockingDeque<File>()

    static void main(String... arguments) {
        arguments.each {
            def initialFile = new File(it)
            if(initialFile.exists()){
                if(initialFile.isDirectory()){
                    folders.push(initialFile)
                    executor.execute(processFolder)
                } else {
                    files.add(initialFile)
                    executor.execute(processFile)
                }
            } else {
                println "$it could not be found"
            }
        }

        executor.execute(processFolder)

        while(!executor.isShutdown()) {
            message()
            sleep 250
        }
        message()
        println ""
    }
    static def processFile = {
        if(files.poll()?.delete()){
            deletedFileCount.incrementAndGet()
        }
    } as Runnable

    static def processFolder = {
        openFolderCount.incrementAndGet()
        try{
            def folder = folders.poll()
            folder?.listFiles()?.each {
                if(it.isDirectory()){
                    folders.push(it)
                    executor.execute(processFolder)
                } else {
                    files.add(it)
                    executor.execute(processFile)
                }
                folder.deleteOnExit()
            }
        } catch (Exception e) {
            println "Caught Random Exception"
            println e
        } finally {
            if(openFolderCount.decrementAndGet() == 0 && folders.isEmpty()){
                executor.shutdown()
            }
        }
    } as Runnable

    static void message() {
        print format("\rDeleted Files: %10d  Remaining Files: %10d  Unexplored Folders: %10d  Open Folders: %1d", deletedFileCount.get(), files.size(), folders.size(), openFolderCount.get())
    }
}

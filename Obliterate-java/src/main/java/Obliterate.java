import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iain_Henderson Created: Nov 3, 2016
 */
public class Obliterate {

    private static final AtomicInteger deletedFileCount = new AtomicInteger();
    private static final AtomicInteger openFolderCount = new AtomicInteger();
    private static final ExecutorService executor = Executors.newWorkStealingPool();
    private static final LinkedBlockingDeque<File> files = new LinkedBlockingDeque<>();
    private static final LinkedBlockingDeque<File> folders = new LinkedBlockingDeque<>();

    private static final Logger logger =
        Logger.getLogger(new Object() {}.getClass().getEnclosingClass().getName());

    /**
     * @param args Command line arguments
     */
    public static void main(final String[] args) {
        Obliterate runner = new Obliterate();
        for (String arg : args) {
            try {
                File next = new File(arg);
                if (next.isDirectory()) {
                    Obliterate.folders.offer(next); // delete folders in the order they are specified
                    Obliterate.executor.execute(runner::processFolder);
                }
                else {
                    Obliterate.files.offer(next);
                    Obliterate.executor.execute(runner::processFile);
                }
            }
            catch (Exception e) {
                warn(arg + " could not be found");
                System.exit(1);
            }
        }

        // if there are no folders here, then there never will be
        if (Obliterate.folders.size() + openFolderCount.get() == 0) {
            Obliterate.executor.shutdown();
        }

        message();

        while (!Obliterate.executor.isTerminated()) {
            message();
            try {
                Thread.sleep(250);
            }
            catch (InterruptedException e) {
            }
        }

        message();

        System.out.println();
    }

    private void processFile() {
        File file = files.poll();
        if (file != null) {
            try {
                Files.delete(file.toPath());
                Obliterate.deletedFileCount.incrementAndGet();
            }
            catch (IOException ignored) {
            }
        }
    }

    private void processFolder() {
        Obliterate.openFolderCount.incrementAndGet();
        try {
            final File folder = Obliterate.folders.poll();
            if (folder != null) {
                File[] nexts = folder.listFiles();
                if (nexts != null) { // Sometimes the folders don't really exist
                    if (nexts.length == 0) {
                        if (!folder.delete()) {
                            warn(() -> "Could not delete " + folder.getAbsolutePath());
                        }
                    }
                    else {
                        for (File next : nexts) {
                            if (next.isDirectory()) {
                                Obliterate.folders.push(next);
                                Obliterate.executor.execute(this::processFolder);
                            }
                            else {
                                Obliterate.files.offer(next);
                                Obliterate.executor.execute(this::processFile);
                            }
                        }
                    }
                }
                folder.deleteOnExit();
            }
        }
        catch (Exception e) {
            warn("Caught random exception");
            warn(() -> e.toString());
        }
        finally {
            if (Obliterate.openFolderCount.decrementAndGet() == 0 && Obliterate.folders.isEmpty()) {
                Obliterate.executor.shutdown();
            }
        }
    }

    private static void message() {
        String message = String.format(
            "\rDeleted Files: %10d   Remaining Files: %10d  Unexplored Folders: %10d  Open Folders: %1d",
            Obliterate.deletedFileCount.get(), Obliterate.files.size(), Obliterate.folders.size(),
            Obliterate.openFolderCount.get());
        System.out.print(message);
    }

    private static void warn(final String message) {
        logger.warning(message);
    }

    private static void warn(final Supplier<String> message) {
        if (logger.isLoggable(Level.WARNING)) {
            warn(message.get());
        }
    }
}

//
//  Node.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/22/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

class Node<Equatable>{
    var nextNode: Node<Equatable>?
    var previousNode: Node<Equatable>?
    var value: Equatable
    
    init(_ value: Equatable, previous: Node<Equatable>? = nil, next: Node<Equatable>? = nil){
        self.value = value
        if((previous) != nil){
            self.previousNode = previous
        }
        if((next) != nil){
            self.nextNode = next
        }
    }
    
    func get() -> Equatable {
        return value
    }
    
    func next(_ next: Node<Equatable>? = nil) -> Node<Equatable>?{
        return nextNode
    }
    
    func previous() -> Node<Equatable>?{
        return previousNode
    }
}

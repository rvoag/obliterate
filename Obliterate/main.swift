//
//  main.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/21/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

for argument in CommandLine.arguments.dropFirst(){
    var isFolder = ObjCBool(false);
    do{
        print(argument);
        if(FileManager.default.fileExists(atPath: argument, isDirectory: &isFolder)){
            if(isFolder).boolValue{
                Obliterate.processFolder(argument);
            } else {
                Obliterate.processFile(argument);
            }
        }
    }
}

Obliterate.message();
while(!Obliterate.complete()){
    Obliterate.message();
    sleep(1);
}
Obliterate.message();

Obliterate.cleanup();

print();
exit(EXIT_SUCCESS);

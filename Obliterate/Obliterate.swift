//
//  Obliterate.swift
//  Obliterate
//
//  Created by Iain Henderson on 10/21/16.
//  Copyright © 2016 Iain Henderson. All rights reserved.
//

import Foundation

let openFolders = Counter();
let deletedFiles = Counter();

let files = OperationQueue();
let folders = OperationQueue();
let fileManager = FileManager.default;

var nonEmptyFolders = Array<String>();

class Obliterate {
    class func processFolder(_ path:String) {
        do {
            var isFolder = ObjCBool(false);
            openFolders.increment();
            let children = try fileManager.contentsOfDirectory(atPath: path);
            if(children.isEmpty){
                try fileManager.removeItem(atPath: path);
            } else {
                for child in children{
                    fileManager.fileExists(atPath: path + "/" + child, isDirectory: &isFolder);
                    if(isFolder).boolValue{
                        if(files.operationCount > 0){
                            files.waitUntilAllOperationsAreFinished();
                        }
                        folders.addOperation{processFolder(path + "/" + child)};
                    } else {
                        files.addOperation{processFile(path + "/" + child)};
                    }
                }
                nonEmptyFolders.insert(path, at: 0);
            }
        } catch {
            
        }
        openFolders.decrement();
    }
    
    class func processFile(_ path:String) {
        if(fileManager.isDeletableFile(atPath: path)){
            do {
                try fileManager.removeItem(atPath: path);
                deletedFiles.increment();
            } catch {
                
            }
        }
    }
    
    class func complete() -> Bool{
        return folders.operationCount + files.operationCount == 0;
    }
    
    class func cleanup(){
        let total = nonEmptyFolders.count;
        
        print();
        
        while(!nonEmptyFolders.isEmpty){
            let folder = nonEmptyFolders.removeFirst();
            do {
                try fileManager.removeItem(atPath:folder);
            } catch {
                nonEmptyFolders.append(folder);
            }
            fputs(String(format: "\r" +
                "Deleting Empty Folders: %d out of %d",
                         total - nonEmptyFolders.count,
                         total), stdout);
        }
    }
    
    class func message(){
        let message = String(format:
            "\r"
                + "Deleted Files: %10d  "
                + "Remaining Files: %10d  "
                + "Unexplored Folders: %10d  "
                + "Open Folders: %3d  "
                + "Folders to Delete: %10d",
                             deletedFiles.get(),
                             files.operationCount,
                             folders.operationCount,
                             openFolders.get(),
                             nonEmptyFolders.count);
        fputs(message, stdout);
    }
}
